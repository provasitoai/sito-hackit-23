Warmup
======

:slug: warmup
:navbar_sort: 5
:lang: it

Cosa sono
"""""""""""""""""""""""""""""""""""""""

I warmup sono eventi "preparatori" ad hackmeeting. Avvengono in giro per l'Italia, e possono trattare gli argomenti più disparati.

Proporre un warmup
"""""""""""""""""""""""""""""""""""""""

Vuoi fare un warmup? ottimo!

* iscriviti alla `mailing list di hackmeeting <https://www.autistici.org/mailman/listinfo/hackmeeting>`_.
* scrivi in mailing list riguardo al tuo warmup: non c'è bisogno di alcuna "approvazione ufficiale", ma segnalarlo in lista è comunque un passaggio utile per favorire dibattito e comunicazione.


Elenco
"""""""""""""""""""""""""""""""""""""""
.. contents:: :local:

==========================


Hackrocchio 
___________
10 Giugno 2023 `@ <https://www.mezcalsquat.net/>`_, Collegno (Torino)
`HackЯocchio <https://hackrocchio.org>`_


Conzalab
---------
16 giugno 2023

Presentazione  CryptoBluff + Lab Elettro Art Attack
---------------------------------------------------

Lunedì 17 Luglio @ Circolo Anarchico Berneri, Bologna (BO)
<https://balotta.org/event/crypt-or-bluff-warm-up-hackmeeting>


Electric dreams
---------------
Da venerdi 28 luglio a domenica 30 luglio 2023 c/o laboratorio e deposito musei MIAI e MusIF Edificio Ex-CUD – Via C.B. Cavour 4, Rende (CS)
<https://miai.musif.eu/electric-dreams-terza-edizione/>

