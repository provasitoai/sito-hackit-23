Contact
###########

:slug: contact
:navbar_sort: 6
:lang: en

**Mailing List**

There is a `mailing list <https://www.autistici.org/mailman/listinfo/hackmeeting>`_ where you can ask for info and follow the discussions about the meeting. It is mostly in italian but feel free to ask questions in english.

**IRC**

There is also an IRC (Internet Relay Chat) channel where discuss and chat with other participants: connect to server ``irc.autistici.org`` and join channel ``#hackit99`` (again, it will be mostly in italian, but english speakers are welcome).

If you prefer XMPP/Jabber, you can reach the same channel as room ``#hackit99@mufhd0.esiliati.org`` (please
include the hash)
