Programma
===========

:slug: schedule
:navbar_sort: 3
:lang: it

.. 
   Gli audio dell'hackmeeting sono disponibili `qui
   <https://archive.org/search.php?query=creator%3A%28radiowombatfirenze%29%20AND%20subject%3A%28hackmeeting%202019%29>`_.

Per seguire il calendario con più comodità:

 * `aggiungilo ad android <https://ggt.gaa.st/#url=https://it.hackmeeting.org/schedule.ics>`_
 * `aggiungilo al tuo calendario desktop <webcals://it.hackmeeting.org/schedule.ics>`_ (ad esempio Thunderbird)
 * `URL grezzo <https://it.hackmeeting.org/schedule.ics>`_

Il programma è soggetto a variazioni continue: vieni ad hackmeeting e vivitelo!

Fatti coraggio, `rispondi alla call for contents <{filename}call.md>`_, proponi il tuo contenuto in `mailing list <{filename}contatti.rst>`_: crea un nuovo thread
dedicato alla tua proposta. Nel subject inserisci ``[TALK]``
(ad esempio ``[TALK] come sbucciare le mele con un cluster di GPU``) così che sia facile ritrovarlo per chi è
interessato.

.. 
   decommenta questa riga e quella sopra per far riapparire la griglia

.. talkgrid::
    :lang: it

.. talklist::
    :lang: it

