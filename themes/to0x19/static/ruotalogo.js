function menu_get() {
    return document.querySelector('#menu-row')
}
function menu_is_shown() {
    return (window.getComputedStyle(menu_get()).display !== 'none')
}
function menu_hide() {
  menu_get().style.display = 'none'
  document.querySelector('#overlay').style.height = '0'
}
function menu_show() {
  menu_get().style.display = 'block'
  document.querySelector('#overlay').style.height = '100%'
}
document.addEventListener('DOMContentLoaded', function() {
  var logo = document.getElementById('logo-img')
  logo.classList.add('rotate')
  function ruotaLogo() {
    logo.style.transform = 'rotate(' + -window.pageYOffset/2+'deg)'
  }
  function scrollSetup() {
    "use strict";
    var last_known_scroll_position = 0
    var ticking = false

    window.addEventListener('scroll', function (e) {
      last_known_scroll_position = window.scrollY

      if (!ticking) {
        window.requestAnimationFrame(function () {

          ruotaLogo()
          ticking = false;
        });

        ticking = true;
      }
    })
  }
  setTimeout(function () {
    logo.classList.remove('rotate')
    scrollSetup()
  }, 1000)
  logo.addEventListener('click', function (evt) {
    if (!menu_is_shown()) {
      menu_show()
    } else {
      menu_hide()
    }
  })

  document.querySelector('#overlay').addEventListener('click', function (evt) {
    menu_hide()
  })
})
